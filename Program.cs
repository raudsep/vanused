﻿using System;

namespace vanused
{
    class Program
    {
        static void Main(string[] args)
        {
            const int ARV = 20;
            string[] nimed = new string[ARV];
            int[] vanused = new int[ARV];

            int opilasteArv = 0;
            for (; opilasteArv < ARV; opilasteArv++)
            {

                Console.WriteLine("Mis nimi?");       //kysime nime
                nimed[opilasteArv] = Console.ReadLine();

                if (nimed[opilasteArv] == "") break;            //kui tyhi vastus, siis enam edasi ei kysi

                Console.Write($"Mis on õpilase {nimed[opilasteArv]} vanus ");         //siis kysime vanuse
                vanused[opilasteArv] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine($"õpilasi on {opilasteArv}");

            double vanusteSumma = 0;
            for (int i = 0; i < opilasteArv; i++) vanusteSumma += vanused[i];
            double keskmineVanus = vanusteSumma / opilasteArv;
            Console.WriteLine($"Keskmine vanus on {keskmineVanus:F2}");




        }

            
            
                

            }



}
    

